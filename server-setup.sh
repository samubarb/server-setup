#!/usr/bin/env sh

echo "Writing ~/.hushlogin..."
touch ~/.hushlogin

echo "Disabling standby..."
sudo systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target

echo "Updating and upgrading packages..."
sudo apt -qq update
sudo apt -qq upgrade -y
sudo apt -qq autoremove

echo "Installing ZSH, neofetch, and powerline..."
sudo apt install -y git zsh neofetch powerline fonts-powerline 
git config --global color.ui auto

echo "Installinf python3 and related..."
sudo apt install -y python3 python3-venv

echo "Installing Oh-My-ZSH..."
git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh

echo "Setting up .zshrc..."
cp ./config/zshrc ~/.zshrc

echo "Setting ZSH as default shell..."
sudo chsh -s $(which zsh)

echo "Installing rsub..."
sudo wget -O /usr/local/bin/rsub https://raw.github.com/aurora/rmate/master/rmate
sudo chmod +x /usr/local/bin/rsub

# Finished
echo "Rebooting..."
sudo reboot
