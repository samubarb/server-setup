# Server Setup Script

Useful script to automate the installation of some software on a fresh Ubuntu Server installation. 

## What's in it

It basically installs `zsh` shell because I love it and `rsub` to better work on files through an SSH connection.

I normally use this script on an IoT class of devices, such as a Raspberry Pi or an old netbook, which serve me in small house projects.

## Sample Files
The `config` folder contains some configuration files as is, such as `.zshrc`, automatically copied by the script and pre-configured as I like them.
In addition there are, in `template` folder, some other configuration files, such as `netplan`-related, that requires a manual *ad hoc* configuration, so the files should be considered as mere templates.